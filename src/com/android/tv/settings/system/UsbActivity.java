package com.android.tv.settings.system;

import android.app.Activity;
import android.content.ContentResolver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.provider.Settings;
import android.util.Log;

import com.android.tv.settings.R;
import com.android.tv.settings.dialog.DialogFragment;
import com.android.tv.settings.dialog.DialogFragment.Action;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class UsbActivity extends Activity implements Action.Listener {
    private static final String TAG = "UsbSettings";
    private static final String HOST_MODE = "1";
    private static final String SLAVE_MODE = "2";
    private static final String SYS_FILE = "/sys/bus/platform/drivers/usb20_otg/force_usb_mode";
    private static final String PREFERENCE_KEY = "usb_mode";
    private static final String ACTION_SLAVE_ON = "slave_on";
    private static final String ACTION_SLAVE_OFF = "slave_off";
	private boolean usbOn;
    private File mFile = null;
    private String mMode = null;
    private DialogFragment mDialogFragment;


    private static int getUSBEnabled(ContentResolver contentResolver) {
        return Settings.Global.getInt(contentResolver, Settings.Global.USB_MASS_STORAGE_ENABLED, 1);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDialogFragment = new DialogFragment.Builder()
                .title(getString(R.string.connectivity_usb))
                .breadcrumb(getString(R.string.header_category_device))
                .iconResourceId(R.drawable.ic_settings_usb)
                .iconBackgroundColor(getResources().getColor(R.color.icon_background))
                .actions(getActions()).build();
        DialogFragment.add(getFragmentManager(), mDialogFragment);
        mFile = new File(SYS_FILE);
        int usbValue = getUSBEnabled(getContentResolver());
        if (usbValue == 1) {
            usbOn = true;
        } else {
            usbOn = false;
        }
    }



    private ArrayList<Action> getActions() {

        ArrayList<Action> actions = new ArrayList<DialogFragment.Action>();
        actions.add(new Action.Builder()
                .key(HOST_MODE)
                .title(getString(R.string.usb_host))
                .checked(usbOn)
                .checkSetId(1)
                .build());
        actions.add(new Action.Builder()
                .key(SLAVE_MODE)
                .title(getString(R.string.usb_slave))
                .checked(!usbOn)
                .checkSetId(1)
                .build());
        return actions;
    }

    public void onActionClicked(Action action) {
        Log.d("TVSettings", "Click detected" + action.getKey());
        int keyVal = Integer.parseInt(action.getKey());
        setUSBEnabled(keyVal);
    }

    private void setUSBEnabled(int value) {
        Log.d("TVSettings", "Writing to file " + mFile + " of " + Integer.toString(value));
        Settings.Global.putInt(getContentResolver(), Settings.Global.USB_MASS_STORAGE_ENABLED, value);
        System.setProperty("persist.usb.enable",Integer.toString(value));
        Write2File(mFile, Integer.toString(value));
    }

    private String ReadFromFile(File file) {
        Log.d(TAG, "ReadFromFile, reading " + file);
        if ((file != null) && file.exists()) {
            try {
                FileInputStream fin = new FileInputStream(file);
                BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
                String config = reader.readLine();
                fin.close();
                Log.d(TAG, "ReadFromFile, config is " + config);
                return config;
            } catch (IOException e) {
                e.printStackTrace();

            }
        }

        return null;
    }

    private void Write2File(File file, String mode) {
        Log.d(TAG, "Write2File,write mode = " + mode);
        if ((file == null) || (!file.exists()) || (mode == null)) return;

        try {
            FileOutputStream fout = new FileOutputStream(file);
            PrintWriter pWriter = new PrintWriter(fout);
            pWriter.println(mode);
            pWriter.flush();
            pWriter.close();
            fout.close();
        } catch (IOException re) {
            Log.d(TAG, "write error:" + re);
            return;
        }
    }

}